/**********************Marine Areas**************************/
/area/sulaco
	name = "\improper Sulaco"
	icon_state = "centcom"

/area/sulaco/medical/general

/area/sulaco/medical/chemistry
	name = "\improper Sulaco Chemistry"
	icon_state = "chem"

/area/sulaco/medical/surgery
	name = "\improper Sulaco Surgery Ward"
	icon_state = "surgery"

/area/sulaco/medical/genetics
	name = "\improper Sulaco Genetics Lab"
	icon_state = "genetics"

/area/sulaco/medical/storage

/area/sulaco/research/xenocontainment

/area/sulaco/general/hanger

/area/sulaco/general/briefing_room

/area/sulaco/command/logistics
	name = "\improper Sulaco Logistics And Bridge"
	icon_state = "bridge"

/area/shuttle/marine_a
	name = "\improper Marine Transport Shuttle One"
	music = "music/escape.ogg"

/area/shuttle/marine_a/sulaco
	icon_state = "shuttle2"

/area/shuttle/marine_a/station
	icon_state = "shuttle"

/area/shuttle/marine_b
	name = "\improper Marine Transport Shuttle Two"
	music = "music/escape.ogg"

/area/shuttle/marine_b/sulaco
	icon_state = "shuttle2"

/area/shuttle/marine_b/station
	icon_state = "shuttle"


